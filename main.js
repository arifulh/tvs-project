// 1. The system should be able to validate the user’s selection.
// 2. The content displayed should be responsive for mobile and desktop.
// 3. Provide access to the see the code and a link to see the page working.

const data = {
  "products": [
    {
      "id": 1,
      "name": "Adult Male Bike",
      "price": 20.50,
      "image": "http://via.placeholder.com/250x250?text=Adult%20Male%20Bike",
      "product_type": "bike"
    },
    {
      "id": 2,
      "name": "Adult Female Bike",
      "price": 20.50,
      "image": "http://via.placeholder.com/250x250?text=Adult%20Female%20Bike",
      "product_type": "bike"
    },
    {
      "id": 3,
      "name": "Kids Unisex Bike",
      "price": 12.75,
      "image": "http://via.placeholder.com/250x250?text=Kids%20Unisex%20Bike",
      "product_type": "bike"
    },
    {
      "id": 4,
      "name": "Adult Unisex Helmet",
      "price": 4.00,
      "image": "http://via.placeholder.com/250x250?text=Adult%20Unisex%20Helmet",
      "product_type": "accessory"
    },
    {
      "id": 5,
      "name": "Kids Unisex Helmet",
      "price": 3.50,
      "image": "http://via.placeholder.com/250x250?text=Kids%20Unisex%20Helmet",
      "product_type": "accessory"
    },
    {
      "id": 6,
      "name": "Insurance",
      "price": 9.99,
      "image": "http://via.placeholder.com/250x250?text=Insurance",
      "product_type": "addon"
    }
  ]
}


class RentalItem {
  constructor(data) {
    this.id = data.id
    this.name = data.name
    this.price = data.price
    this.image = data.image
    this.name = data.name
    this.product_type = data.product_type
  }

  render(flag,qty) {
    let div = document.createElement('div'),
    removeButton = `<span data-id="${this.id}" data-product_type="${this.product_type}" class="app-remove-item">REMOVE</span>`,
    addButton = `<span data-id="${this.id}" data-product_type="${this.product_type}" class="app-add-item">ADD</span>`,
    html = `<div class="rental-item">
      <div>
        <span class="item-name">${this.name}</span>
        ${ qty ? `(qty: <span>${qty}</span>)` : '' }
        ${ flag !== "cart-item" ? addButton : ''}
        ${ flag !== "inventory-item" ? removeButton : ''}
      </div>
      <div>${this.price}</div>
      <div><img src="${this.image}" /></div>
    </div>
    `
    div.innerHTML = html.trim()
    return div.firstChild;
  }
} 

class RentalApp {

  constructor(data) {
    this.data = data;
    this.inventory = []
    this.cart = {}
    this.total = 0;
  }

  init() {
    this.setInventory()
    this.render()
  }

  setInventory() {
    let inventory = this.inventory;
    this.data.forEach(( d ) => {
      inventory.push(new RentalItem(d))
    })
  }

  getItemById(id) {
    return this.inventory.filter((item) => {
      return item.id == id
    })[0]
  }

  addItemToCart(id) {
    let confirmation = confirm("Are you sure you want to ADD this item?") 
    if (confirmation === false) return;

    let item = this.getItemById(id)
    this.cart[item.id] = this.cart[item.id] || { id: id, quantity: 0 }
    this.cart[item.id].quantity++;

    this.total += item.price; 
    this.renderCart()
  }

  removeItemFromCart(id) {
    let confirmation = confirm("Are you sure you want to REMOVE this item?") 
    if (confirmation === false) return;

    let item = this.getItemById(id)
    if ( this.cart[item.id] && this.cart[item.id].quantity > 0 ) {
      this.cart[item.id].quantity--
    }

    this.total = this.total - item.price; 
    this.renderCart()
  }

  renderTotal() {
    document.querySelector('#app-total').innerHTML = `Total: $${this.total}`
  }

  renderInventory() {
    let inventoryEl = document.querySelector("#app-inventory");

    this.inventory.forEach(( d ) => {
      inventoryEl.append(d.render('inventory-item'))
    }) 

    document.querySelectorAll('.app-add-item').forEach(( item ) => {
      item.addEventListener('click', (e) => {
        let id = e.target.attributes['data-id']
        this.addItemToCart(id.value)
      })
    })

  }

  renderCart() {
    let cartEl = document.querySelector("#app-cart");
    document.querySelector("#app-cart").innerHTML = ""

    for (let item in this.cart) {
      let itm = this.getItemById(this.cart[item].id),
          quantity = this.cart[item].quantity

      if (quantity > 0) {
        cartEl.append(itm.render('cart-item', quantity))
      }

      this.renderTotal()
    }

    document.querySelectorAll('.app-remove-item').forEach(( item ) => {
      item.addEventListener('click', (e) => {
        let id = e.target.attributes['data-id']
        this.removeItemFromCart(id.value)
      })
    })

  }

  render() {
    this.renderTotal()
    this.renderInventory()
    this.renderCart()
  }
}

(function(RentalApp) {
  const app = new RentalApp(data.products)
  app.init()
}(RentalApp))